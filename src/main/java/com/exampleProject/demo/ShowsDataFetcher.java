package com.exampleProject.demo;

import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsMutation;
import com.netflix.graphql.dgs.DgsQuery;
import com.netflix.graphql.dgs.InputArgument;
import graphql.com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;

import static com.exampleProject.demo.Models.*;

@DgsComponent
public class ShowsDataFetcher {

    private List<Models.Show> shows = Lists.newArrayList(
            new Models.Show("Stranger Things", 2016),
            new Models.Show("Ozark", 2017),
            new Models.Show("The Crown", 2016),
            new Models.Show("Dead to Me", 2019),
            new Models.Show("Orange is the New Black", 2013)
    );

    @DgsQuery
    public List<Models.Show> shows(@InputArgument String titleFilter) {
        if (titleFilter == null) {
            return shows;
        }

        return shows.stream().filter(s -> s.getTitle().contains(titleFilter))
                .collect(Collectors.toList());
    }

    @DgsMutation
    Models.Show addShow(@InputArgument(value = "show") Models.Show show) {
        this.shows.add(show);
        return show;
    }
}
