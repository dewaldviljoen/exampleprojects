package com.exampleProject.demo

class Models {
    data class Show(val title: String, val releaseYear: Int)
}
